import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import { routes } from './router/'
import VueMoment from 'vue-moment'

const router = new VueRouter({
	routes: routes // short for `routes: routes`
})


Vue.use(VueRouter)
Vue.use(VueMoment)

new Vue({
	el: '#app',
	router: router,
	render: h => h(App)
});