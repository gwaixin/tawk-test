import CategoryPage from './CategoryPage.vue'
import HomePage from './HomePage.vue'
import SearchPage from './SearchPage.vue'

export const routes = [
    { path: '/category/:id', name: 'category', component: CategoryPage },
    { path: '/search/:key', name: 'search', component: SearchPage },
	{ path: '', name: 'home', component: HomePage },
]